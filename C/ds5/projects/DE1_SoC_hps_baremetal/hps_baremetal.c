/*
 * hps_baremetal.c
 *
 *  Created on: 5 okt. 2015
 *      Author: GilrainB
 */

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "hps_baremetal.h"
#include "alt_clock_manager.h" // for the hps timer and delay
#include "alt_globaltmr.h" // for the hps timer and delay
#include "alt_generalpurpose_io.h" // for the GPIO's
#include "socal/alt_gpio.h" // for the GPIO's
#include "socal/socal.h"

int main() {
	printf("DE1-SoC bare-metal demo\n");
	//setup_peripherals();
	uint32_t hex_counter = 0;

	while (true) {
		//handle_hex_displays(&hex_counter);
		handle_hps_led();
		delay_us(ALT_MICROSECS_IN_A_SEC / 10);
	}
	return 0;
}

// fpga buttons can be found at an offset from the base of the lightweight HPS-to-FPGA bridge
void *fpga_buttons = ALT_LWFPGASLVS_ADDR + BUTTONS_0_BASE;

bool is_fpga_button_pressed(uint32_t button_number) {
// buttons are active-low
	return ((~alt_read_word(fpga_buttons)) & (1 << button_number));
}

void set_hex_displays(uint32_t value) {
	char current_char[2] = " \0";
	char hex_counter_hex_string[HEX_DISPLAY_COUNT + 1];
	// get hex string representation of input value on HEX_DISPLAY_COUNT 7-segment displays
	snprintf(hex_counter_hex_string, HEX_DISPLAY_COUNT + 1, "%0*x",
			HEX_DISPLAY_COUNT, (unsigned int) value);

	uint32_t hex_display_index = 0;
	for (hex_display_index = 0; hex_display_index < HEX_DISPLAY_COUNT;
			hex_display_index++) {

		current_char[0] = hex_counter_hex_string[HEX_DISPLAY_COUNT
				- hex_display_index - 1];

		// get decimal representation for this 7-segment display
		uint32_t number = (uint32_t) strtol(current_char, NULL, 16);

		// use lookup table to find active-low value to represent number on the 7-segment display
		uint32_t hex_value_to_write = hex_display_table[number];
		alt_write_word(fpga_hex_displays[hex_display_index],
				hex_value_to_write);
	}
}

void setup_hps_timer() {
	assert(ALT_E_SUCCESS == alt_globaltmr_init());
} /* The HPS doesn't have a sleep() function like the Nios II, so we can make one
 * by using the global timer.
 */
void delay_us(uint32_t us) {
	uint64_t start_time = alt_globaltmr_get64();
	uint32_t timer_prescaler = alt_globaltmr_prescaler_get() + 1;
	uint64_t end_time;
	alt_freq_t timer_clock;
	assert(ALT_E_SUCCESS == alt_clk_freq_get(ALT_CLK_MPU_PERIPH, &timer_clock));
	end_time = start_time
			+ us * ((timer_clock / timer_prescaler) / ALT_MICROSECS_IN_A_SEC);
	while (alt_globaltmr_get64() < end_time) {
// polling wait
	}
}


// HPS KEY en LED lezen/schrijven
// |=============|==========|==============|==========|
// | Signal Name | HPS GPIO | Register/bit | Function |
// |=============|==========|==============|==========|
// | HPS_LED | GPIO53 | GPIO1[24] | I/O |
// | HPS_KEY | GPIO54 | GPIO1[25] | I/O |
// |=============|==========|==============|==========|

#define HPS_LED_IDX (ALT_GPIO_1BIT_53) // GPIO53
#define HPS_LED_PORT (alt_gpio_bit_to_pid(HPS_LED_IDX)) // ALT_GPIO_PORTB
#define HPS_LED_PORT_BIT (alt_gpio_bit_to_port_pin(HPS_LED_IDX)) // 24 (from GPIO1[24])
#define HPS_LED_MASK (1 << HPS_LED_PORT_BIT)

#define HPS_KEY_IDX (ALT_GPIO_1BIT_54) // GPIO54
#define HPS_KEY_PORT (alt_gpio_bit_to_pid(HPS_KEY_IDX)) // ALT_GPIO_PORTB
#define HPS_KEY_PORT_BIT (alt_gpio_bit_to_port_pin(HPS_KEY_IDX)) // 25 (from GPIO1[25])
#define HPS_KEY_MASK (1 << HPS_KEY_PORT_BIT)

void setup_hps_gpio() {
	uint32_t hps_gpio_config_len = 2;
	ALT_GPIO_CONFIG_RECORD_t hps_gpio_config[] = { { HPS_LED_IDX,
			ALT_GPIO_PIN_OUTPUT, 0, 0, ALT_GPIO_PIN_DEBOUNCE,
			ALT_GPIO_PIN_DATAZERO }, { HPS_KEY_IDX, ALT_GPIO_PIN_INPUT, 0, 0,
			ALT_GPIO_PIN_DEBOUNCE, ALT_GPIO_PIN_DATAZERO } };
	assert(ALT_E_SUCCESS == alt_gpio_init());
	assert(ALT_E_SUCCESS == alt_gpio_group_config(hps_gpio_config, hps_gpio_config_len));
}


void handle_hps_led() {
	uint32_t hps_gpio_input = alt_gpio_port_data_read(HPS_KEY_PORT,
			HPS_KEY_MASK);
// HPS_KEY is active-low
	bool toggle_hps_led = (~hps_gpio_input & HPS_KEY_MASK);
	if (toggle_hps_led) {
		uint32_t hps_led_value = alt_read_word(ALT_GPIO1_SWPORTA_DR_ADDR);
		hps_led_value >>= HPS_LED_PORT_BIT;
		hps_led_value = !hps_led_value;
		hps_led_value <<= HPS_LED_PORT_BIT;
		assert(ALT_E_SUCCESS == alt_gpio_port_data_write(HPS_LED_PORT, HPS_LED_MASK, hps_led_value));
	}
}
