/*
 * test.c
 *
 *  Created on: 8 okt. 2015
 *      Author: GilrainB
 */

#include "test.h"


int main() {

	setup_hps_gpio();
	setup_hps_timer();

	printf(	"This program tests the DE1-SoC.\n");
	printf(	"char: %i, short: %i, int: %i,\nlong int: %i, long long: %i,\nfloat: %i, double: %i.\n\n",
			sizeof(char), sizeof(short), sizeof(int), sizeof(long int),
			sizeof(long long), sizeof(float), sizeof(double));

	printf("Address of the CAN0 device: %X - %X , range: %X.(should be 1FF)\n",
			// (Maak een int van) Het adres van( de waarde van ((de char pointer naar) eigenlijke adres))
			(int)&(*((char*)ALT_CAN0_ADDR)), (int)&(*((char*)ALT_CAN0_UB_ADDR)),
			((int)((char*)ALT_CAN0_UB_ADDR) - (int) ALT_CAN0_OFST));

	for (int i = 0; i < 6; i++)
		SET_HEX_DISPLAY(i, i + 1);

	for (int d = 0; d < 6; d++) // tijd: 6*(16*0.05 sec) = 4,8 sec
		for (int i = 0; i < 16; i++) { // print op alle displays 0 tot en met F

			//SET_HPS_LED( 1); // WERKT NIET
			toggle_gpio_pin(HPS_LED_IDX);
			SET_HEX_DISPLAY(d, i);
			SLEEP(110);
		}

	for (int halfSec = 4 * 2; halfSec > 0; halfSec--) {
		// toggle the HPS_LED for 4 seconds, duty cycle 50% every half second
		toggle_gpio_pin(HPS_LED_IDX);
		SLEEP(500);
	}

	int x = 3;
	printf("Test program succeeded. Will start tests on CAN in %i seconds:\n", x);
	for (; x > 0; x--) {
		printf("%i...\n", x);
		SLEEP(1000);
	}

	can0Device = initCAN(ALT_CAN_CAN0);
	can_test_mode(&can0Device, true);

	//can1Device = initCAN(ALT_CAN_CAN1);

	char* phrases[4];
	phrases[0] = "Sending";
	phrases[1] = "Receivn";
	phrases[2] = "msg was";
	phrases[3] = "success";

	int i = 0;
	printf("%s & %s started.\n",phrases[0], phrases[1]);

	while (1) {

		canData_t * message = (canData_t*)phrases[i];
		i++;
		if (i>=4) i =0;

		// send data from can0
		can_send(&can0Device, 0, message);
		SLEEP(750);
		toggle_gpio_pin(HPS_LED_IDX);
		canData_t receivedMessage = can_receive(&can0Device);
		SLEEP(750);

		// get can status and print to screen
		/*
		ALT_CAN_STATUS_t stats;
		alt_can_status_get(can0Device,stats);
		printf("CAN0 status: RX:%s, TX:%s, Warn:%s\n",(stats.rx_success)?"OK":"NOK",(stats.tx_success)?"OK":"NOK",(stats.warning)?"Y":"N");

		alt_can_int_status_get(can1Device, stats);
		printf("CAN1 status: RX:%s, TX:%s, Warn:%s\n",(stats.rx_success)?"OK":"NOK",(stats.tx_success)?"OK":"NOK",(stats.warning)?"Y":"N");

		SLEEP(500);
		*/
	}

}

///////////////////////
// Program functions //
///////////////////////

ALT_CAN_DEV_t initCAN(ALT_CAN_CTLR_t canDevEnum) {
	ALT_CAN_DEV_t canDevice;
	int msg_obj_nr, id,	mask;
	ALT_STATUS_CODE status;
	//canDevice = (ALT_CAN_DEV_t) malloc(sizeof(ALT_CAN_DEV_t));

	/* According to Cyclone V Device Handbook, the mailboxes
	 * need to be configured before resetting/initializing the CAN device:
	 * "The host processor must set the MsgVal bit of all unused message objects to 0
	 * before it resets the initialization bit (Init) of the CAN control register (CCTRL)
	 * to initialize the CAN controller.
	 * (MsgVal must also be set to 0 when the message object
	 * is no longer in use.)"
	 */
	// Set up a write-message object/mailbox
	msg_obj_nr = 1;// WARNING: starts from 1 and not 0
	id = 0; // send broadcast
	mask = 0;
	//status = alt_can_mailbox_init(canDevice,msg_obj_nr,id,mask,ALT_CAN_TMOD_TX,ALT_CAN_FIFO_MODE_SINGLE_MSG);
	assert(status == ALT_E_SUCCESS);
	// Set up a read-message object/mailbox
	msg_obj_nr = 2;
	id = 0; // Receive all CAN messages
	mask = 0;
	//alt_can_mailbox_init(canDevice,msg_obj_nr,id,mask,ALT_CAN_TMOD_RX,ALT_CAN_FIFO_MODE_SINGLE_MSG);


	alt_can_init(canDevEnum, &canDevice);
	// baudrate
	alt_can_baudrate_set(&canDevice, CAN_BAUD_RATE);
	// bittiming? not needed
	// Test mode? not needed
	// object parameters, mask, arbitration, control?

	// enable CAN interrupt service routine
	//alt_can_int_enable(&canDevice, mask);

	return canDevice;
}

ALT_STATUS_CODE can_send(ALT_CAN_DEV_t* device, short address,
		canData_t* message) {
	ALT_STATUS_CODE ans;

	printf("Send: '%s'\n", (char*) message);

#ifndef USE_MESSAGE_BOX
	ans = alt_can_if_data_set(device, ALT_CAN_INTERFACE_WRITE,
			message->word[0],
			message->word[1]);
#else
	// TODO: Use message send box
	// alt_can_message_put

#endif
	return ans;
}

ALT_STATUS_CODE can_test_mode(ALT_CAN_DEV_t* device, bool enable) {
	ALT_STATUS_CODE status;
	if (enable) {
		// Enable test mode
		status = alt_can_test_mode_enable(device, ALT_CAN_TEST_LOOP_BACK);
		printf("CAN dev \"%s\" was put into loopback mode.\n",
				(IS_CAN0(device)) ? "CAN0" : "CAN1");
	} else {
		status = alt_can_test_mode_disable(device);
	}
	return status;
}

// CAN interrupt service routine
canData_t can_receive(ALT_CAN_DEV_t* device) {
	canData_t data;

#ifndef USE_MESSAGE_BOX
	// Polling/get last message
	alt_can_if_data_get(device, ALT_CAN_INTERFACE_READ,
			data.word, data.word + 1);
#else
	// Message box
	canData_t* receiveData;

	memcp(&data, receiveData,sizeof(canData_t));
#endif

	printf("Received data: \"%s\"\n", data.byte);
	return data;
}


////////////////////////////////////////////////////////////////////
////////////////////////// (lib)Functions //////////////////////////
////////////////////////////////////////////////////////////////////
bool is_fpga_button_pressed(uint32_t button_number) {
// buttons are active-low
	return ((~alt_read_word(fpga_buttons)) & (1 << button_number));
}

void setup_hps_timer() {
	assert(ALT_E_SUCCESS == alt_globaltmr_init());
}

/**
 *  The HPS doesn't have a sleep() function like the Nios II, so we can make one
 * by using the global timer.
 **/
void delay_us(uint32_t us) {
	uint64_t start_time = alt_globaltmr_get64();
	uint32_t timer_prescaler = alt_globaltmr_prescaler_get() + 1;
	uint64_t end_time;
	alt_freq_t timer_clock;
	assert(ALT_E_SUCCESS == alt_clk_freq_get(ALT_CLK_MPU_PERIPH, &timer_clock));
	end_time = start_time
			+ us * ((timer_clock / timer_prescaler) / ALT_MICROSECS_IN_A_SEC);
	while (alt_globaltmr_get64() < end_time) {
// polling wait
	}
}
////////////////////////
//////// GPIO's ////////
////////////////////////

/**
 * Setup the user led and button gpio pins
 */
void setup_hps_gpio() {
	uint32_t hps_gpio_config_len = 2;
	ALT_GPIO_CONFIG_RECORD_t hps_gpio_config[] = { { HPS_LED_IDX,
			ALT_GPIO_PIN_OUTPUT, 0, 0, ALT_GPIO_PIN_DEBOUNCE,
			ALT_GPIO_PIN_DATAZERO }, { HPS_KEY_IDX, ALT_GPIO_PIN_INPUT, 0, 0,
			ALT_GPIO_PIN_DEBOUNCE, ALT_GPIO_PIN_DATAZERO } };

	assert(ALT_E_SUCCESS == alt_gpio_init());

	assert(ALT_E_SUCCESS == alt_gpio_group_config(hps_gpio_config, hps_gpio_config_len));
}

void setup_gpio_pin(uint32_t gpioNumber, bool inOut){
	enum ALT_GPIO_PIN_DIR_e inputOutput =
			(inOut)? ALT_GPIO_PIN_INPUT : ALT_GPIO_PIN_OUTPUT;

	assert(ALT_E_SUCCESS == alt_gpio_bit_config(gpioNumber, inputOutput, 0, 0,
			ALT_GPIO_PIN_DEBOUNCE, ALT_GPIO_PIN_DATAZERO));
}

bool get_gpio_pin(uint32_t pinNo) {
	uint32_t port = alt_gpio_bit_to_pid(pinNo);
	uint32_t port_bit = alt_gpio_bit_to_port_pin(pinNo);
	uint32_t mask = 1 << port_bit;

	uint32_t hps_gpio_input = alt_gpio_port_data_read(port, mask); // actually reading the GPIO

	return ((hps_gpio_input) ? true : false);
}

void set_gpio_pin(uint32_t pinNo, bool onOff) {
	uint32_t port = alt_gpio_bit_to_pid(pinNo);
	uint32_t port_bit = alt_gpio_bit_to_port_pin(pinNo);
	uint32_t mask = 1 << port_bit;


	// TODO: Kan geen gpio setten op de HPS, code hieronder faalt
	assert( ALT_E_SUCCESS == alt_gpio_port_data_write(port, mask, (onOff)?(~0):0));
}

void toggle_gpio_pin(uint32_t pinNo) {

	uint32_t port = alt_gpio_bit_to_pid(pinNo);
	uint32_t port_bit = alt_gpio_bit_to_port_pin(pinNo);
	uint32_t mask = 1 << port_bit;

	uint32_t hps_gpio_input = alt_gpio_port_data_read(port, mask);
	// The set/clear bit is already at the right spot, so no bitshifts are needed

	hps_gpio_input = ~hps_gpio_input;
	hps_gpio_input &= mask;

	assert( ALT_E_SUCCESS == alt_gpio_port_data_write(port, mask, hps_gpio_input));

	/*bool toggl = get_gpio_pin(pinNo);

	if (toggl){
		set_gpio_pin(pinNo, 0);
	} else {
		set_gpio_pin(pinNo, 1);
	}//*/
}

