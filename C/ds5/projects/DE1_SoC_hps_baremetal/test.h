/*
 * test.h
 *
 *  Created on: 8 okt. 2015
 *      Author: GilrainB
 */

#ifndef TEST_H_
#define TEST_H_

////////////////////
///// Includes /////
////////////////////


// Standard stuff //
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

// For the system
#include "socal/hps.h"			// Internal HPS peripherals
#include "socal/socal.h"		// Convenience Macros for handling data registers
#include "hps_soc_system.h"	// FPGA components connected to the HPS
#include "alt_globaltmr.h"		// Global timer connected to both ARM-cores
#include "alt_clock_manager.h"	// for using the enumeration of clock sources "ALT_CLK_e"
#include "alt_generalpurpose_io.h"// For using pins 53 and 54 (HPS led and key)

// For the application
#include "alt_can.h"

////////////////////
////// Macros //////
////////////////////

#undef USE_MESSAGE_BOX
#define LOGGING // for all CAN

// Numbers
#define CAN_BAUD_RATE 1000000 // 1 Mbit

// Convenience macros(that are like
#define SLEEP(ms) delay_us(ms * 1000)
#define SET_HEX_DISPLAY(display, pattern) alt_write_word(fpga_hex_displays[display], hex_display_table[pattern])
#define CLEAR_HEX_DISPLAY(display) alt_write_word(fpga_hex_displays[display], HEX_0_RESET_VALUE)
#define IS_CAN0(device) ((int)device->location == ALT_CAN_CAN0)
#define SET_HPS_LED(onOff) set_gpio_pin( HPS_LED_IDX, onOff)

// The 7-segment display is active low
#define HEX_DISPLAY_COUNT (6)
#define HEX_DISPLAY_CLEAR (0x7F)
#define HEX_DISPLAY_ZERO (0x40)
#define HEX_DISPLAY_ONE (0x79)
#define HEX_DISPLAY_TWO (0x24)
#define HEX_DISPLAY_THREE (0x30)
#define HEX_DISPLAY_FOUR (0x19)
#define HEX_DISPLAY_FIVE (0x12)
#define HEX_DISPLAY_SIX (0x02)
#define HEX_DISPLAY_SEVEN (0x78)
#define HEX_DISPLAY_EIGHT (0x00)
#define HEX_DISPLAY_NINE (0x18)
#define HEX_DISPLAY_A (0x08)
#define HEX_DISPLAY_B (0x03)
#define HEX_DISPLAY_C (0x46)
#define HEX_DISPLAY_D (0x21)
#define HEX_DISPLAY_E (0x06)
#define HEX_DISPLAY_F (0x0E) // The HPS will only use HEX_DISPLAY_COUNT of the 6 7-segment displays#define HEX_COUNTER_MASK ((1 << (4 * HEX_DISPLAY_COUNT)) - 1)


//// GPIO's ////

// HPS User button(KEY8, GPIO1[25], Pin 54) and User LED(LEDG7, GPIO1[24], Pin 53)
#define HPS_LED_IDX		(ALT_GPIO_1BIT_53) // or in a litteral form: 53
#define HPS_KEY_IDX		(ALT_GPIO_1BIT_54) // pin 54

///////////////////
//// Variables ////
///////////////////
const void *fpga_hex_displays[HEX_DISPLAY_COUNT] = { ALT_LWFPGASLVS_ADDR + HEX_0_BASE,
ALT_LWFPGASLVS_ADDR + HEX_1_BASE, ALT_LWFPGASLVS_ADDR + HEX_2_BASE,
ALT_LWFPGASLVS_ADDR + HEX_3_BASE, ALT_LWFPGASLVS_ADDR + HEX_4_BASE,
ALT_LWFPGASLVS_ADDR + HEX_5_BASE };

uint32_t hex_display_table[16] = { HEX_DISPLAY_ZERO, HEX_DISPLAY_ONE,
HEX_DISPLAY_TWO, HEX_DISPLAY_THREE,
HEX_DISPLAY_FOUR, HEX_DISPLAY_FIVE,
HEX_DISPLAY_SIX, HEX_DISPLAY_SEVEN,
HEX_DISPLAY_EIGHT, HEX_DISPLAY_NINE,
HEX_DISPLAY_A, HEX_DISPLAY_B,
HEX_DISPLAY_C, HEX_DISPLAY_D,
HEX_DISPLAY_E, HEX_DISPLAY_F };


// fpga buttons can be found at an offset from the base of the lightweight HPS-to-FPGA bridge
const void *fpga_buttons = ALT_LWFPGASLVS_ADDR + BUTTONS_0_BASE;


/////// CAN ///////
ALT_CAN_DEV_t can0Device, can1Device;
typedef union {
	uint8_t byte[8];
	uint16_t hword[4];
	uint32_t word[2];
	uint64_t dword;
} canData_t;


///////////////////
//// Functions ////
///////////////////

ALT_CAN_DEV_t initCAN(ALT_CAN_CTLR_t canDevEnum);
ALT_STATUS_CODE can_send(ALT_CAN_DEV_t* device, short address, canData_t* message);
ALT_STATUS_CODE can_test_mode(ALT_CAN_DEV_t* device, bool enable);
canData_t can_receive(ALT_CAN_DEV_t* device);

////////////////////////////////////////////////////////////////////
////////////////////////// (lib)Functions //////////////////////////
////////////////////////////////////////////////////////////////////

bool is_fpga_button_pressed(uint32_t);
void setup_hps_timer();
void delay_us(uint32_t us);
// GPIO's:
void setup_hps_gpio(); // user button and led
void setup_gpio_pin(uint32_t gpioNumber, bool inOut);
bool get_gpio_pin(uint32_t pinNo);
void set_gpio_pin(uint32_t pinNo, bool onOff);
void toggle_gpio_pin(uint32_t pinNo);

#endif /* TEST_H_ */
