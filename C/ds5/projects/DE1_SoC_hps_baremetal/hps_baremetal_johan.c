#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

/*
#include "alt_clock_manager.h"
#include "alt_generalpurpose_io.h"
#include "alt_globaltmr.h"
#include "socal/alt_gpio.h"
*/

// These might be useful some day:
//#include "hwlib.h"
//#include "hps_baremetal_johan.h"

#include "alt_can.h"			// Altera CAN controller API from the HWLIB

//#include "socal/socal.h"		// Gives us the alt_read_word()-like utilities
//#include "socal/hps.h"			// HPS system hard peripherals
#include "../hps_soc_system.h"	// FPGA soft peripherals

ALT_CAN_DEV_t can_device;

int main()
{
	char i=0;
	volatile int j = 0;

	printf("App console up and running...\n");


	assert(ALT_E_SUCCESS == alt_can_init(ALT_CAN_CAN0, &can_device));

	printf("CAN device 0 initialised\n");

	// I do not understand the way mask is used
	ALT_CAN_STATUS_t mask;
	mask.bus_off = true;
	mask.err_passive = true;
	mask.last_err_code = 0;
	mask.par_error = false;
	mask.rx_success = false;
	mask.tx_success = false;
	mask.warning = false;

	assert(ALT_E_SUCCESS == alt_can_int_enable(&can_device, (uint8_t)0xFF));
	printf("CAN device 0 enabled.\n");

	assert(ALT_E_SUCCESS == alt_can_test_mode_enable(&can_device, ALT_CAN_TEST_LOOP_BACK));
	printf("CAN device 0 test mode enabled in loopback mode.");



	while(1)
	{
		i++;
		alt_write_word(ALT_LWFPGASLVS_ADDR+HEX_123_PO_BASE,i);//(i+16)<<14  |(i+8)<<7  | (i));
		alt_write_word(ALT_LWFPGASLVS_ADDR+HEX_456_PO_BASE,i ^ 0x55555555);//(i+128)<<14 |(i+64)<<7 | (i+32));
		printf("Keys %X\n",		(unsigned int)alt_read_word(ALT_LWFPGASLVS_ADDR+KEYS_PI_BASE));
		printf("Switches %X\n",	(unsigned int)alt_read_word(ALT_LWFPGASLVS_ADDR+SWITCHES_PI_BASE));

		for (j=0;j<1000000;j++);
	}

	return 0;
}

