################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/hwmgr/alt_16550_uart.c \
../src/hwmgr/alt_address_space.c \
../src/hwmgr/alt_cache.c \
../src/hwmgr/alt_can.c \
../src/hwmgr/alt_dma.c \
../src/hwmgr/alt_dma_program.c \
../src/hwmgr/alt_ecc.c \
../src/hwmgr/alt_generalpurpose_io.c \
../src/hwmgr/alt_globaltmr.c \
../src/hwmgr/alt_i2c.c \
../src/hwmgr/alt_interrupt.c \
../src/hwmgr/alt_mmu.c \
../src/hwmgr/alt_nand.c \
../src/hwmgr/alt_qspi.c \
../src/hwmgr/alt_reset_manager.c \
../src/hwmgr/alt_sdmmc.c \
../src/hwmgr/alt_spi.c \
../src/hwmgr/alt_system_manager.c \
../src/hwmgr/alt_timers.c \
../src/hwmgr/alt_watchdog.c 

S_SRCS += \
../src/hwmgr/alt_bridge_f2s_gnu.s 

OBJS += \
./src/hwmgr/alt_16550_uart.o \
./src/hwmgr/alt_address_space.o \
./src/hwmgr/alt_bridge_f2s_gnu.o \
./src/hwmgr/alt_cache.o \
./src/hwmgr/alt_can.o \
./src/hwmgr/alt_dma.o \
./src/hwmgr/alt_dma_program.o \
./src/hwmgr/alt_ecc.o \
./src/hwmgr/alt_generalpurpose_io.o \
./src/hwmgr/alt_globaltmr.o \
./src/hwmgr/alt_i2c.o \
./src/hwmgr/alt_interrupt.o \
./src/hwmgr/alt_mmu.o \
./src/hwmgr/alt_nand.o \
./src/hwmgr/alt_qspi.o \
./src/hwmgr/alt_reset_manager.o \
./src/hwmgr/alt_sdmmc.o \
./src/hwmgr/alt_spi.o \
./src/hwmgr/alt_system_manager.o \
./src/hwmgr/alt_timers.o \
./src/hwmgr/alt_watchdog.o 

C_DEPS += \
./src/hwmgr/alt_16550_uart.d \
./src/hwmgr/alt_address_space.d \
./src/hwmgr/alt_cache.d \
./src/hwmgr/alt_can.d \
./src/hwmgr/alt_dma.d \
./src/hwmgr/alt_dma_program.d \
./src/hwmgr/alt_ecc.d \
./src/hwmgr/alt_generalpurpose_io.d \
./src/hwmgr/alt_globaltmr.d \
./src/hwmgr/alt_i2c.d \
./src/hwmgr/alt_interrupt.d \
./src/hwmgr/alt_mmu.d \
./src/hwmgr/alt_nand.d \
./src/hwmgr/alt_qspi.d \
./src/hwmgr/alt_reset_manager.d \
./src/hwmgr/alt_sdmmc.d \
./src/hwmgr/alt_spi.d \
./src/hwmgr/alt_system_manager.d \
./src/hwmgr/alt_timers.d \
./src/hwmgr/alt_watchdog.d 


# Each subdirectory must supply rules for building sources it contributes
src/hwmgr/%.o: ../src/hwmgr/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	arm-altera-eabi-gcc -mcpu=cortex-a9 -Dsoc_cv_av -I"C:\altera\15.0\embedded\ip\altera\hps\altera_hps\hwlib\include" -I"C:\altera\15.0\embedded\ip\altera\hps\altera_hps\hwlib\include\soc_cv_av" -O0 -g -Wall -std=c99 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/hwmgr/%.o: ../src/hwmgr/%.s
	@echo 'Building file: $<'
	@echo 'Invoking: GCC Assembler'
	arm-altera-eabi-gcc -mcpu=cortex-a9 -g -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


