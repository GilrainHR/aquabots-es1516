	soc_system u0 (
		.buttons_0_external_connection_export  (<connected-to-buttons_0_external_connection_export>),  //  buttons_0_external_connection.export
		.clk_clk                               (<connected-to-clk_clk>),                               //                            clk.clk
		.hex_0_external_connection_export      (<connected-to-hex_0_external_connection_export>),      //      hex_0_external_connection.export
		.hex_1_external_connection_export      (<connected-to-hex_1_external_connection_export>),      //      hex_1_external_connection.export
		.hex_2_external_connection_export      (<connected-to-hex_2_external_connection_export>),      //      hex_2_external_connection.export
		.hex_3_external_connection_export      (<connected-to-hex_3_external_connection_export>),      //      hex_3_external_connection.export
		.hex_4_external_connection_export      (<connected-to-hex_4_external_connection_export>),      //      hex_4_external_connection.export
		.hex_5_external_connection_export      (<connected-to-hex_5_external_connection_export>),      //      hex_5_external_connection.export
		.hps_0_ddr_mem_a                       (<connected-to-hps_0_ddr_mem_a>),                       //                      hps_0_ddr.mem_a
		.hps_0_ddr_mem_ba                      (<connected-to-hps_0_ddr_mem_ba>),                      //                               .mem_ba
		.hps_0_ddr_mem_ck                      (<connected-to-hps_0_ddr_mem_ck>),                      //                               .mem_ck
		.hps_0_ddr_mem_ck_n                    (<connected-to-hps_0_ddr_mem_ck_n>),                    //                               .mem_ck_n
		.hps_0_ddr_mem_cke                     (<connected-to-hps_0_ddr_mem_cke>),                     //                               .mem_cke
		.hps_0_ddr_mem_cs_n                    (<connected-to-hps_0_ddr_mem_cs_n>),                    //                               .mem_cs_n
		.hps_0_ddr_mem_ras_n                   (<connected-to-hps_0_ddr_mem_ras_n>),                   //                               .mem_ras_n
		.hps_0_ddr_mem_cas_n                   (<connected-to-hps_0_ddr_mem_cas_n>),                   //                               .mem_cas_n
		.hps_0_ddr_mem_we_n                    (<connected-to-hps_0_ddr_mem_we_n>),                    //                               .mem_we_n
		.hps_0_ddr_mem_reset_n                 (<connected-to-hps_0_ddr_mem_reset_n>),                 //                               .mem_reset_n
		.hps_0_ddr_mem_dq                      (<connected-to-hps_0_ddr_mem_dq>),                      //                               .mem_dq
		.hps_0_ddr_mem_dqs                     (<connected-to-hps_0_ddr_mem_dqs>),                     //                               .mem_dqs
		.hps_0_ddr_mem_dqs_n                   (<connected-to-hps_0_ddr_mem_dqs_n>),                   //                               .mem_dqs_n
		.hps_0_ddr_mem_odt                     (<connected-to-hps_0_ddr_mem_odt>),                     //                               .mem_odt
		.hps_0_ddr_mem_dm                      (<connected-to-hps_0_ddr_mem_dm>),                      //                               .mem_dm
		.hps_0_ddr_oct_rzqin                   (<connected-to-hps_0_ddr_oct_rzqin>),                   //                               .oct_rzqin
		.hps_0_io_hps_io_can0_inst_RX          (<connected-to-hps_0_io_hps_io_can0_inst_RX>),          //                       hps_0_io.hps_io_can0_inst_RX
		.hps_0_io_hps_io_can0_inst_TX          (<connected-to-hps_0_io_hps_io_can0_inst_TX>),          //                               .hps_io_can0_inst_TX
		.hps_0_io_hps_io_gpio_inst_GPIO53      (<connected-to-hps_0_io_hps_io_gpio_inst_GPIO53>),      //                               .hps_io_gpio_inst_GPIO53
		.hps_0_io_hps_io_gpio_inst_GPIO54      (<connected-to-hps_0_io_hps_io_gpio_inst_GPIO54>),      //                               .hps_io_gpio_inst_GPIO54
		.leds_0_external_connection_export     (<connected-to-leds_0_external_connection_export>),     //     leds_0_external_connection.export
		.pll_0_sdram_clk                       (<connected-to-pll_0_sdram_clk>),                       //                    pll_0_sdram.clk
		.reset_reset_n                         (<connected-to-reset_reset_n>),                         //                          reset.reset_n
		.switches_0_external_connection_export (<connected-to-switches_0_external_connection_export>)  // switches_0_external_connection.export
	);

