library ieee;
use ieee.std_logic_1164.all;

entity de1_soc_top_level is

	port 
	(
		CLOCK_50 : in std_logic;
		
		-- SDRAM
		
		DRAM_ADDR : out std_logic_vector(12 downto 0);
		DRAM_BA : out std_logic_vector(1 downto 0);
		DRAM_CAS_N : out std_logic;
		DRAM_CKE : out std_logic;
		DRAM_CLK : out std_logic;
		DRAM_CS_N : out std_logic;
		DRAM_DQ : inout std_logic_vector(15 downto 0);

		DRAM_LDQM : out std_logic;
		DRAM_RAS_N : out std_logic;
		DRAM_UDQM : out std_logic;
		DRAM_WE_N : out std_logic;
		
		-- SEG7
		HEX0 : out std_logic_vector(6 downto 0);
		HEX1 : out std_logic_vector(6 downto 0);
		HEX2 : out std_logic_vector(6 downto 0);
		HEX3 : out std_logic_vector(6 downto 0);
		HEX4 : out std_logic_vector(6 downto 0);
		HEX5 : out std_logic_vector(6 downto 0);
		
		-- KEY_n
		KEY : in std_logic_vector(3 downto 0);
		
		-- LED
		LEDR : out std_logic_vector(9 downto 0);
		
		-- SW
		SW : in std_logic_vector(9 downto 0);
		
		-- CAN0
		CAN0_RX : in std_logic; -- Needs to be mapped to Pin B22, HPS_GPIO61
		CAN0_TX : out std_logic;-- Needs to be mapped to Pin G22, HPS_GPIO62
		
		-- HPS
		HPS_DDR3_ADDR : out std_logic_vector(14 downto 0);
		HPS_DDR3_BA : out std_logic_vector(2 downto 0);
		HPS_DDR3_CAS_N : out std_logic;
		HPS_DDR3_CK_N : out std_logic;
		HPS_DDR3_CK_P : out std_logic;
		HPS_DDR3_CKE : out std_logic;
		HPS_DDR3_CS_N : out std_logic;
		HPS_DDR3_DM : out std_logic_vector(3 downto 0);
		HPS_DDR3_DQ : inout std_logic_vector(31 downto 0);
		HPS_DDR3_DQS_N : inout std_logic_vector(3 downto 0);
		HPS_DDR3_DQS_P : inout std_logic_vector(3 downto 0);
		HPS_DDR3_ODT : out std_logic;
		HPS_DDR3_RAS_N : out std_logic;
		HPS_DDR3_RESET_N : out std_logic;
		HPS_DDR3_RZQ : in std_logic;
		HPS_DDR3_WE_N : out std_logic;
		HPS_KEY : inout std_logic;
		HPS_LED : inout std_logic
	);

end entity de1_soc_top_level;

architecture rtl of de1_soc_top_level is

    	component soc_system is
		port (
			buttons_0_external_connection_export  : in    std_logic_vector(3 downto 0)  := (others => 'X'); -- export
			clk_clk                               : in    std_logic                     := 'X';             -- clk
			hex_0_external_connection_export      : out   std_logic_vector(6 downto 0);                     -- export
			hex_1_external_connection_export      : out   std_logic_vector(6 downto 0);                     -- export
			hex_2_external_connection_export      : out   std_logic_vector(6 downto 0);                     -- export
			hex_3_external_connection_export      : out   std_logic_vector(6 downto 0);                     -- export
			hex_4_external_connection_export      : out   std_logic_vector(6 downto 0);                     -- export
			hex_5_external_connection_export      : out   std_logic_vector(6 downto 0);                     -- export
			hps_0_ddr_mem_a                       : out   std_logic_vector(14 downto 0);                    -- mem_a
			hps_0_ddr_mem_ba                      : out   std_logic_vector(2 downto 0);                     -- mem_ba
			hps_0_ddr_mem_ck                      : out   std_logic;                                        -- mem_ck
			hps_0_ddr_mem_ck_n                    : out   std_logic;                                        -- mem_ck_n
			hps_0_ddr_mem_cke                     : out   std_logic;                                        -- mem_cke
			hps_0_ddr_mem_cs_n                    : out   std_logic;                                        -- mem_cs_n
			hps_0_ddr_mem_ras_n                   : out   std_logic;                                        -- mem_ras_n
			hps_0_ddr_mem_cas_n                   : out   std_logic;                                        -- mem_cas_n
			hps_0_ddr_mem_we_n                    : out   std_logic;                                        -- mem_we_n
			hps_0_ddr_mem_reset_n                 : out   std_logic;                                        -- mem_reset_n
			hps_0_ddr_mem_dq                      : inout std_logic_vector(31 downto 0) := (others => 'X'); -- mem_dq
			hps_0_ddr_mem_dqs                     : inout std_logic_vector(3 downto 0)  := (others => 'X'); -- mem_dqs
			hps_0_ddr_mem_dqs_n                   : inout std_logic_vector(3 downto 0)  := (others => 'X'); -- mem_dqs_n
			hps_0_ddr_mem_odt                     : out   std_logic;                                        -- mem_odt
			hps_0_ddr_mem_dm                      : out   std_logic_vector(3 downto 0);                     -- mem_dm
			hps_0_ddr_oct_rzqin                   : in    std_logic                     := 'X';             -- oct_rzqin
			
			hps_0_io_hps_io_can0_inst_RX          : in    std_logic                     := 'X';             -- hps_io_can0_inst_RX
			hps_0_io_hps_io_can0_inst_TX          : out   std_logic;                                        -- hps_io_can0_inst_TX
			
			hps_0_io_hps_io_gpio_inst_GPIO53      : inout std_logic                     := 'X';             -- hps_io_gpio_inst_GPIO53
			hps_0_io_hps_io_gpio_inst_GPIO54      : inout std_logic                     := 'X';             -- hps_io_gpio_inst_GPIO54
			leds_0_external_connection_export     : out   std_logic_vector(9 downto 0);                     -- export
			pll_0_sdram_clk                       : out   std_logic;                                        -- clk
			reset_reset_n                         : in    std_logic                     := 'X';             -- reset_n
			switches_0_external_connection_export : in    std_logic_vector(9 downto 0)  := (others => 'X')  -- export
		);
	end component soc_system;
	 
begin

	u0 : component soc_system
		port map (

			buttons_0_external_connection_export=> KEY,
			clk_clk							=> CLOCK_50,
			
			hex_0_external_connection_export=> HEX0,
			hex_1_external_connection_export=> HEX1,
			hex_2_external_connection_export=> HEX2,
			hex_3_external_connection_export=> HEX3,
			hex_4_external_connection_export=> HEX4,
			hex_5_external_connection_export=> HEX5,
			
			hps_0_ddr_mem_a					=> HPS_DDR3_ADDR,
			hps_0_ddr_mem_ba				=> HPS_DDR3_BA,
			hps_0_ddr_mem_ck				=> HPS_DDR3_CK_P,
			hps_0_ddr_mem_ck_n				=> HPS_DDR3_CK_N,
			hps_0_ddr_mem_cke				=> HPS_DDR3_CKE,
			hps_0_ddr_mem_cs_n				=> HPS_DDR3_CS_N,
			hps_0_ddr_mem_ras_n				=> HPS_DDR3_RAS_N,
			hps_0_ddr_mem_cas_n				=> HPS_DDR3_CAS_N,
			hps_0_ddr_mem_we_n				=> HPS_DDR3_WE_N,
			hps_0_ddr_mem_reset_n			=> HPS_DDR3_RESET_N,
			hps_0_ddr_mem_dq				=> HPS_DDR3_DQ,
			hps_0_ddr_mem_dqs				=> HPS_DDR3_DQS_P,
			hps_0_ddr_mem_dqs_n				=> HPS_DDR3_DQS_N,
			hps_0_ddr_mem_odt				=> HPS_DDR3_ODT,
			hps_0_ddr_mem_dm				=> HPS_DDR3_DM,
			hps_0_ddr_oct_rzqin				=> HPS_DDR3_RZQ,
			
			hps_0_io_hps_io_can0_inst_RX	=> CAN0_RX, -- Opgezochte pin: E24(HPS_GPIO65) of B22(HPS_GPIO61) - zouden 61 en 62 moeten zijn volgens Qsys pin assignment
			hps_0_io_hps_io_can0_inst_TX	=> CAN0_TX, -- Opgezochte pin: D24(HPS_GPIO66) of G22(HPS_GPIO62)
			
			hps_0_io_hps_io_gpio_inst_GPIO53	=> HPS_LED,
			hps_0_io_hps_io_gpio_inst_GPIO54	=> HPS_KEY,

			leds_0_external_connection_export => LEDR,
			pll_0_sdram_clk 				=> DRAM_CLK,
			reset_reset_n 					=> '1',
			
			switches_0_external_connection_export => SW
        );

end rtl;
